package nl.ordina.wordcount;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WordFrequencyAnalyzerTest {
    WordFrequencyAnalyzerImpl wordFrequencyAnalyzer;

    @BeforeEach
    void setUp() {
        wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();
    }

    /*
    calculateHighestFrequency tests
     */
    @Test
    @DisplayName("calculateHighestFrequency: Test with a simple sentence containing the word 'it' four times in different letter-casing")
    void testHighestWordFrequencyOfSentence() {
        assertEquals(4, wordFrequencyAnalyzer.calculateHighestFrequency("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I don't really need iT, I'm going to leave it there."));
    }

    @Test
    @DisplayName("calculateHighestFrequency: Test with a simple sentence containing the word 'Java' three times including delimiter characters")
    void testHighestWordFrequencyOfSentenceWithDelimiters() {
        assertEquals(3, wordFrequencyAnalyzer.calculateHighestFrequency("Java works fine and is rather fast. " +
                "This is understandable because its a java-based solution, but since I don't really need them, going to leave the java.jee dependency there."));
    }

    @Test
    @DisplayName("calculateHighestFrequency: Test with a simple sentence containing the word 'Java' AND 'it' three times")
    void testHighestWordFrequencyOfSimpleSentenceDuplicateResult() {
        assertEquals(3, wordFrequencyAnalyzer.calculateHighestFrequency("Java works fine and is rather fast. " +
                "This is understandable because it is a java-based solution, but since I don't really need them, going to leave the java.jee dependency there."));
    }

    @Test
    @DisplayName("calculateHighestFrequency: Test that zero is returned if an empty string is provided")
    void testHighestWordFrequencyEmptyInput() {
        assertEquals(0, wordFrequencyAnalyzer.calculateHighestFrequency(""));
    }

    @Test
    @DisplayName("calculateHighestFrequency: Test that zero is returned if no words are provided")
    void testHighestWordFrequencyDelimiterInput() {
        assertEquals(0, wordFrequencyAnalyzer.calculateHighestFrequency(" - , 6 9 6"));
    }

    /*
     calculateFrequencyForWord tests
     */
    @Test
    @DisplayName("calculateFrequencyForWord: Test that the word 'it' has a frequency of four, also testing case-insensitivity")
    void testWordFrequencyForWordCaseInsensitivity() {
        assertEquals(4, wordFrequencyAnalyzer.calculateFrequencyForWord("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I don't really need iT, I'm going to leave it there.", "it"));
    }

    @Test
    @DisplayName("calculateFrequencyForWord: Test that the word 'iT' also has a frequency of four")
    void testWordFrequencyForSuppliedWordCaseInsensitivity() {
        assertEquals(4, wordFrequencyAnalyzer.calculateFrequencyForWord("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I don't really need IT, I'm going to leave it there.", "iT"));
    }

    @Test
    @DisplayName("calculateFrequencyForWord: Test that the word 'Ordina' has a frequency of zero")
    void testWordFrequencyForNonOccuringWord() {
        assertEquals(0, wordFrequencyAnalyzer.calculateFrequencyForWord("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I don't really need iT, I'm going to leave it there.", "Ordina"));
    }
}
