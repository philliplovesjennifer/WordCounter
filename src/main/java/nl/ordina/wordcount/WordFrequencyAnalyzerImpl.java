package nl.ordina.wordcount;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.MediaType;

import java.util.*;

@ApplicationPath("rest")
public class WordFrequencyAnalyzerImpl extends Application implements WordFrequencyAnalyzer {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/calculateHighestFrequency")
    public int calculateHighestFrequency(@PathParam("text") String text) {
        return getHighestFrequencyFromMap(getWordFrequencyMap(text));
    }

    public int calculateFrequencyForWord(String text, String word) {
        return text.split("(?i)\\b" + word).length - 1;
    }

    public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {
        WordFrequencyImpl wordFrequency = new WordFrequencyImpl("poep", 1);
        return Arrays.asList(wordFrequency);
    }

    private Map<String, Integer> getWordFrequencyMap(String text) {
        Map<String, Integer> map = new HashMap<>();;
        // Create a stream of the provided text, split by everything except alphabetical letters, also filtering out empty text.
        // Afterwards, loop through every word and add an entry to the map of the lowercased word and increment the frequency
        Arrays.stream(text.split("[^A-z]")).filter(t -> !t.isEmpty())
                .forEach(w -> map.merge(w.toLowerCase(), 1, Integer::sum));
        return map;
    }

    private int getHighestFrequencyFromMap(Map<String, Integer> wordFrequency) {
        Optional<Map.Entry<String, Integer>> highestFrequency = wordFrequency.entrySet().stream()
                .max(Comparator.comparing(Map.Entry::getValue));
        return highestFrequency.isPresent() ? highestFrequency.get().getValue() : 0;
    }
}
